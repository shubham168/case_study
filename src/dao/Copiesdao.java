package dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import utils.Dbutil;

public class Copiesdao implements Closeable {
	private Connection connection;
	private PreparedStatement stmtInsert;
	private PreparedStatement stmtUpdateRack;
	private PreparedStatement stmtUpdatestatus;

	private PreparedStatement stmtSelect;

	public Copiesdao() throws Exception {
		this.connection = Dbutil.getConnection();
		this.stmtInsert = this.connection.prepareStatement("INSERT INTO copies(bookid,rack,status) VALUES(?,?,?)");
		this.stmtUpdateRack = this.connection.prepareStatement("UPDATE copies SET rack=? WHERE bookid=?");
		this.stmtUpdatestatus = this.connection.prepareStatement("UPDATE copies SET status=? WHERE bookid=?");
		this.stmtSelect = this.connection.prepareStatement("SELECT * FROM copies where bookid = ? and status = ?");

	}

	// insert
	public int insert(int bookid, int rack, String status) throws Exception {
		this.stmtInsert.setInt(1, bookid);
		this.stmtInsert.setInt(2, rack);
		this.stmtInsert.setString(3, status);
		return this.stmtInsert.executeUpdate();
	}

	// update rack
	public int updateRack(int bookId, int rack) throws Exception {
		this.stmtUpdateRack.setFloat(1, rack);
		this.stmtUpdateRack.setInt(2, bookId);
		return this.stmtUpdateRack.executeUpdate();
	}

	public int updatestatus(int bookId, String status) throws Exception {
		this.stmtUpdatestatus.setInt(2, bookId);
		this.stmtUpdatestatus.setString(1, status);
		return this.stmtUpdatestatus.executeUpdate();
	}

	// get copies
	public List<Integer> getcopies(int bookid) throws Exception {
		List<Integer> bookList = new ArrayList<>();
		this.stmtSelect.setInt(1, bookid);
		this.stmtSelect.setString(2, "available");
		try (ResultSet rs = this.stmtSelect.executeQuery();) {
			while (rs.next())

				bookList.add(rs.getInt("bookid"));

		}
		return bookList;
	}

	public void getcopyId(int bookid) throws Exception {
		List<Integer> copyList = new ArrayList<>();
		this.stmtSelect.setInt(1, bookid);
		this.stmtSelect.setString(2, "available");
		try (ResultSet rs = this.stmtSelect.executeQuery();) {
			while (rs.next()) {

				copyList.add(rs.getInt("id"));
				System.out.println("copyid: " + rs.getInt("id"));
			}

		}
		if (copyList.isEmpty()) {
			System.out.println("No book avaliable");
		}

	}

	@Override
	public void close() throws IOException {
		try {
			this.stmtInsert.close();
			this.stmtUpdateRack.close();
			this.stmtUpdatestatus.close();
			this.stmtSelect.close();
			this.connection.close();
		} catch (SQLException cause) {
			throw new IOException(cause); // Exception Chaining
		}
	}
}
