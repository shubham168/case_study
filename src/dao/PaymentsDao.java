package dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pojo.Book;
import utils.Dbutil;

public class PaymentsDao implements Closeable {
	private Connection connection;
	private PreparedStatement stmtInsert;
	private PreparedStatement stmtUpdateRack;
	private PreparedStatement stmtSelect;

	public PaymentsDao() throws Exception {
		this.connection = Dbutil.getConnection();
		this.stmtInsert = this.connection.prepareStatement(
				"INSERT INTO payments (userid,amount,transaction_time,nextpayment_duedate) VALUES(?,?,CURRENT_TIMESTAMP,adddate(curdate(),30))");
		this.stmtUpdateRack = this.connection.prepareStatement("UPDATE payments SET rack=? WHERE bookid=?");
		this.stmtSelect = this.connection.prepareStatement("SELECT * FROM payments ");

	}

	public int insert(int userid, int amount) throws Exception {
		this.stmtInsert.setInt(1, userid);
		this.stmtInsert.setInt(2, amount);
		return this.stmtInsert.executeUpdate();

	}

	public void getFeesReport() throws Exception {

		try (ResultSet rs = this.stmtSelect.executeQuery();) {
			System.out.println();
			System.out.printf("id amount   transaction_time   nextpayment_duedate        userid\n");

			while (rs.next()) {
				System.out.printf("%-5d%-8d%-20s%-20s%10d\n", rs.getInt("id"), rs.getInt("amount"),
						rs.getDate("transaction_time"), rs.getDate("nextpayment_duedate"), rs.getInt("userid"));
			}
			System.out.println();
		}
	}

	@Override
	public void close() throws IOException {
		try {
			this.stmtInsert.close();
			this.stmtUpdateRack.close();
			this.stmtSelect.close();
			this.connection.close();
		} catch (SQLException cause) {
			throw new IOException(cause); // Exception Chaining
		}
	}
}
