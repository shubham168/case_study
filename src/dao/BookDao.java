package dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pojo.Book;
import utils.Dbutil;

public class BookDao implements Closeable {
	private Connection connection;
	private PreparedStatement stmtInsert;
	private PreparedStatement stmtUpdate;
	private PreparedStatement stmtSelect;
	private PreparedStatement stmtFindBook;

	public BookDao() throws Exception {
		this.connection = Dbutil.getConnection();
		this.stmtInsert = this.connection
				.prepareStatement("INSERT INTO books(book_name,author,subject,price,isbn) VALUES(?,?,?,?,?)");
		this.stmtUpdate = this.connection.prepareStatement("UPDATE books SET price=? WHERE id=?");
		this.stmtSelect = this.connection.prepareStatement("SELECT * FROM books");
		this.stmtFindBook = this.connection.prepareStatement("SELECT * FROM books WHERE book_name=?");

	}

	// insert
	public int insert(Book book) throws Exception {
		this.stmtInsert.setString(1, book.getBookName());
		this.stmtInsert.setString(2, book.getAuthorName());
		this.stmtInsert.setString(3, book.getSubjectName());
		this.stmtInsert.setFloat(4, book.getPrice());
		this.stmtInsert.setInt(5, book.getIsbn());
		return this.stmtInsert.executeUpdate();
	}

	// update
	public int update(int bookId, float price) throws Exception {
		this.stmtUpdate.setFloat(1, price);
		this.stmtUpdate.setInt(2, bookId);
		return stmtUpdate.executeUpdate();
	}
	// delete

	// getBooks
	public List<Book> getBooks() throws Exception {
		List<Book> bookList = new ArrayList<>();
		try (ResultSet rs = this.stmtSelect.executeQuery();) {
			while (rs.next())
				bookList.add(new Book(rs.getInt("id"), rs.getString("subject"), rs.getString("book_name"),
						rs.getString("author"), rs.getFloat("price"), rs.getInt("isbn")));
			bookList.forEach(System.out::println);
		}
		return bookList;
	}

	// getBook
	public List<Book> findBook(String name) throws Exception {
		List<Book> bookList = new ArrayList<>();
		this.stmtFindBook.setString(1, name);
		try (ResultSet rs = this.stmtFindBook.executeQuery();) {
			while (rs.next())
				if (rs.getString("book_name").equalsIgnoreCase(name)) {
					bookList.add(new Book(rs.getInt("id"), rs.getString("subject"), rs.getString("book_name"),
							rs.getString("author"), rs.getFloat("price"), rs.getInt("isbn")));
				}
		}
		return bookList;
	}

	@Override
	public void close() throws IOException {
		try {
			this.stmtInsert.close();
			this.stmtUpdate.close();
			this.stmtSelect.close();
			this.connection.close();
		} catch (SQLException cause) {
			throw new IOException(cause); // Exception Chaining
		}
	}
}
