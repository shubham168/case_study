package dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import utils.Dbutil;

public class IssueRecordDao implements Closeable {
	private Connection connection;
	private PreparedStatement stmtInsert;
	private PreparedStatement stmtUpdate;
	private PreparedStatement stmtSelect;
	public IssueRecordDao() throws Exception{
	this.connection = Dbutil.getConnection();
	this.stmtInsert = this.connection.prepareStatement("INSERT INTO issuerecord (copyid,memberid,issue_date,return_duedate) VALUES(?,?,CURRENT_TIMESTAMP,adddate(curdate(),7))");
	this.stmtUpdate = this.connection.prepareStatement("UPDATE issuerecord set return_date = curdate(), fine_amount =(select (datediff(return_date,return_duedate)-7)*5 from (select * from issuerecord) as k where copyid = ? AND memberid = ? and datediff(return_date,return_duedate)>7) where copyid = ? and memberid = ?");
	this.stmtSelect = this.connection.prepareStatement("SELECT * FROM issuerecord ");

	}
	
	public int insert(int copyid,int userid) throws Exception{
		this.stmtInsert.setInt(1, copyid);
		this.stmtInsert.setInt(2, userid);
		
		return this.stmtInsert.executeUpdate();
		
		
	}
	
	public int returned(int copyid,int userid) throws Exception{
		this.stmtUpdate.setInt(1, copyid);
		this.stmtUpdate.setInt(2, userid);
		this.stmtUpdate.setInt(3, copyid);
		this.stmtUpdate.setInt(4, userid);
		return this.stmtUpdate.executeUpdate();
		
		
	}
	
	public void getFineReport( )throws Exception{
		
		try (ResultSet rs = this.stmtSelect.executeQuery();) {
			System.out.println();

			System.out.printf("id copyId    member_id  issue_date   return_duedate   return_date fine_amount\n" );

			while (rs.next()) {
				System.out.printf("%-5d%-10d%-10d%-15s%-15s%-15s%10d\n", rs.getInt("id"),rs.getInt("copyid"),rs.getInt("memberid"), rs.getDate("issue_date"),rs.getDate("return_duedate"), rs.getDate("return_date"),rs.getInt("fine_amount")  );
			}
			System.out.println();

		}
	}
	
	@Override
	public void close() throws IOException {
		try {
			this.stmtInsert.close();
			this.stmtSelect.close();
			this.stmtUpdate.close();
			this.connection.close();
		} catch (SQLException cause) {
			throw new IOException(cause);	//Exception Chaining
		}
	}
}
