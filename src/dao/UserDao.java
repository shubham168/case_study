package dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import pojo.LIbrarian;
import pojo.Member;
import pojo.Owner;
import pojo.User;
import utils.Dbutil;

public class UserDao implements Closeable{
	private Connection connection;
	private PreparedStatement stmtInsert;
	private PreparedStatement stmtUpdate;
	private PreparedStatement stmtSelect;
	private PreparedStatement stmtFindUser;
	private PreparedStatement stmtUpdateProfile;
	public UserDao() throws Exception {
		this.connection = Dbutil.getConnection();
		this.stmtInsert = this.connection.prepareStatement("INSERT INTO users(name,email,mobile_no,pass,role) VALUES(?,?,?,?,?)");
		this.stmtUpdate = this.connection.prepareStatement("UPDATE users SET pass=? WHERE email=?");
		this.stmtSelect = this.connection.prepareStatement("SELECT * FROM users");
		this.stmtFindUser = this.connection.prepareStatement("SELECT * FROM users WHERE email=? AND pass=?");
		this.stmtUpdateProfile = this.connection.prepareStatement("UPDATE users SET name=? , email=? , mobile_no=? WHERE email=?");
	}
	//insert
	public int insert(User user) throws Exception{
		
		this.stmtInsert.setString(1, user.getName());
		this.stmtInsert.setString(2, user.getEmail());
		this.stmtInsert.setString(3, user.getMob_no());
		this.stmtInsert.setString(4, user.getPass());
		this.stmtInsert.setString(5, user.getRole());

		return this.stmtInsert.executeUpdate();
	}
	//update
	public int update(String email, String pass ) throws Exception{
		this.stmtUpdate.setString(1, email);
		this.stmtUpdate.setString(2, pass);
		return stmtUpdate.executeUpdate();
	}
	
	//getUsers
	public List<User> getUsers( )throws Exception{
		List<User> userList = new ArrayList<>();
		try (ResultSet rs = this.stmtSelect.executeQuery();) {
			while (rs.next()) 
				userList.add(  new User( rs.getInt("userid"), rs.getString("name"), rs.getString("email"), rs.getString("mobile_no"),rs.getString("pass"),rs.getString("role")) );
			userList.forEach(System.out::println);
		}
		return userList;
	}
	
	public User getUser(String[] strings ) throws Exception {
		this.stmtFindUser.setString(1,strings[0] );
		this.stmtFindUser.setString(2,strings[1] );
		try(ResultSet rs = this.stmtFindUser.executeQuery();){
			User u = null;
			
			while(rs.next()) {
				System.out.println(rs.getString("role"));
				if(rs.getString("role").equalsIgnoreCase("owner"))
					u = new Owner( rs.getInt("userid"), rs.getString("name"), rs.getString("email"), rs.getString("mobile_no"),rs.getString("pass"),rs.getString("role"));	
				else if(rs.getString("role").equalsIgnoreCase("member"))
					u = new Member( rs.getInt("userid"), rs.getString("name"), rs.getString("email"), rs.getString("mobile_no"),rs.getString("pass"),rs.getString("role"));
				else if(rs.getString("role").equalsIgnoreCase("librarian"))
					u = new LIbrarian( rs.getInt("userid"), rs.getString("name"), rs.getString("email"), rs.getString("mobile_no"),rs.getString("pass"),rs.getString("role"));
			}
			return u ;	
		}
	}
	
	@Override
	public void close() throws IOException {
		try {
			this.stmtInsert.close();
			this.stmtUpdate.close();
			this.stmtSelect.close();
			this.connection.close();
		} catch (SQLException cause) {
			throw new IOException(cause);	//Exception Chaining
		}
	}
	public void editProfile(String name, String new_email, String mobno,String email) throws Exception{
		this.stmtUpdateProfile.setString(1, name);
		this.stmtUpdateProfile.setString(2, new_email);
		this.stmtUpdateProfile.setString(3, mobno);
		this.stmtUpdateProfile.setString(4, email);
		
		this.stmtUpdateProfile.executeUpdate();
		
	}
}
