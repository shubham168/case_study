package pojo;

import java.util.Scanner;
import dao.BookDao;
import dao.IssueRecordDao;
import dao.PaymentsDao;
import dao.UserDao;
import test.Program;

public class Owner extends User {

	public Owner() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Owner(int userid, String name, String email, String mob_no, String pass, String role) {
		super(userid, name, email, mob_no, pass, role);
		// TODO Auto-generated constructor stub
	}

	public Owner(String name, String email, String mob_no, String pass, String role) {
		super(name, email, mob_no, pass, role);
		// TODO Auto-generated constructor stub
	}

	static Scanner sc = new Scanner(System.in);

	public int menulist() {

		System.out.println("0.Sign out");
		System.out.println("1.Appoint Librarian");
		System.out.println("2.Edit Profile");
		System.out.println("3.Edit Password");
		System.out.println("4.Fees Report");
		System.out.println("5.Fine Report");
		System.out.println("6.Book Categories");
		int choice = sc.nextInt();
		sc.nextLine();
		return choice;
	}

	public void oFunc() {
		int choice = 0;
		while ((choice = menulist()) != 0) {
			switch (choice) {
			case 1: {

				User u = Program.sign_up();
				u.setRole("librarian");
				try (UserDao d = new UserDao()) {
					d.insert(u);
					System.out.println("Librarian Appointed");

				} catch (Exception e) {
					e.printStackTrace();
				}

				System.out.println();
				break;
			}
			case 2: {
				this.editProfile();

				System.out.println();
				break;

			}
			case 3: {
				this.editPassword();
				System.out.println();
				break;

			}

			case 4: {
				try (PaymentsDao d = new PaymentsDao()) {
					d.getFeesReport();

				} catch (Exception e) {
					e.printStackTrace();
				}

				System.out.println();
				break;

			}
			case 5: {
				try (IssueRecordDao d = new IssueRecordDao()) {
					d.getFineReport();
				} catch (Exception e) {
					e.printStackTrace();
				}

				System.out.println();
				break;

			}
			case 6: {
				try (BookDao d = new BookDao()) {
					System.out.println();
					d.getBooks();
					System.out.println();
				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println();
				break;
			}

			}
		}
	}

}
