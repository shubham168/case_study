package pojo;

import java.util.Scanner;

import dao.UserDao;

public class User {
	static int UserId;
	static Scanner sc = new Scanner(System.in);
	String name;
	String email;
	String mob_no;
	String pass;
	String role;

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public User() {

	}

	public User(int userid, String name, String email, String mob_no, String pass, String role) {
		UserId = userid;
		this.name = name;
		this.email = email;
		this.mob_no = mob_no;
		this.pass = pass;
		this.role = role;
	}

	public User(String name, String email, String mob_no, String pass, String role) {
		this.name = name;
		this.email = email;
		this.mob_no = mob_no;
		this.pass = pass;
		this.role = role;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMob_no() {
		return mob_no;
	}

	public void setMob_no(String mob_no) {
		this.mob_no = mob_no;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public int getUserId() {
		return UserId;
	}

	@Override
	public String toString() {
		return String.format("%-10d%-15s%-20s%-15s", UserId, this.getName(), this.getEmail(), this.getMob_no());
	}

	public void editProfile() {
		try (UserDao d = new UserDao()) {
			System.out.println("Enter name");
			String name = sc.nextLine();
			System.out.println("Enter new email");
			String new_email = sc.nextLine();
			System.out.println("Enter mobile number");
			String mobno = sc.nextLine();
			d.editProfile(name, new_email, mobno, this.email);
			System.out.println("Profile changes saved");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void editPassword() {
		try (UserDao d = new UserDao()) {
			System.out.println("Enter new password");
			String pass = sc.nextLine();

			d.update(this.email, pass);
			System.out.println("Password changed ");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
