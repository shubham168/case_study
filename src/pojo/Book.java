package pojo;

import java.util.*;

import dao.BookDao;

public class Book {
	static Scanner sc = new Scanner(System.in);
	private int bookId, isbn;
	private String subjectName, bookName, authorName;
	private float price;

	public Book() {
	}

	public Book(int bookId, String subjectName, String bookName, String authorName, float price, int isbn) {
		this.bookId = bookId;
		this.subjectName = subjectName;
		this.bookName = bookName;
		this.authorName = authorName;
		this.price = price;
		this.isbn = isbn;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getIsbn() {
		return isbn;
	}

	public void setIsbn(int isbn) {
		this.isbn = isbn;
	}

	@Override
	public String toString() {
		return String.format("%-5d%-20s%-55s%-40s%-8.2f%-10d", bookId, subjectName, bookName, authorName, price, isbn);
	}

	public boolean findBook() {
		try (BookDao dao = new BookDao()) {
			System.out.println("Enter the name of the book");
			String name = sc.nextLine();
			System.out.println();
			List<Book> books = dao.findBook(name);
			books.forEach(System.out::println);
			System.out.println();
			return !books.isEmpty();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;

	}
}
