package pojo;

import java.util.List;
import java.util.Scanner;

import dao.*;
import test.Program;

public class LIbrarian extends User {

	public LIbrarian() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LIbrarian(int userid, String name, String email, String mob_no, String pass, String role) {
		super(userid, name, email, mob_no, pass, role);
		// TODO Auto-generated constructor stub
	}

	public LIbrarian(String name, String email, String mob_no, String pass, String role) {
		super(name, email, mob_no, pass, role);
		// TODO Auto-generated constructor stub
	}

	static Scanner sc = new Scanner(System.in);

	public static Book getBook() {
		Book b = new Book();
		System.out.println("Enter the Subject");
		b.setSubjectName(sc.nextLine());
		System.out.println("Enter the name of the book");
		b.setBookName(sc.nextLine());
		System.out.println("Enter the name of the autor");
		b.setAuthorName(sc.nextLine());
		System.out.println("Enter the price of the book");
		b.setPrice(sc.nextFloat());
		System.out.println("Enter the isbn number");
		b.setIsbn(sc.nextInt());

		return b;
	}

	public int menulist() {
		System.out.println("0.Sign Out");
		System.out.println("1.Edit Profile");
		System.out.println("2.Edit Password");
		System.out.println("3.Add member");
		System.out.println("4.Add new Book");
		System.out.println("5.Add new copy");
		System.out.println("6.Find Book");
		System.out.println("7.Issue copy");
		System.out.println("8.Return copy");
		System.out.println("9.Edit book");
		System.out.println("10.Take Payment");
		System.out.println("11.Check Avaliablity");
		System.out.println("12.Change rack");
		int choice = sc.nextInt();
		sc.nextLine();
		return choice;
	}

	public void lFunc() {
		int choice = 0;
		while ((choice = menulist()) != 0) {
			switch (choice) {
			case 1: {
				editProfile();

				System.out.println();
				break;
			}
			case 2: {
				this.editPassword();

				System.out.println();
				break;

			}
			case 3: {

				User u = Program.sign_up();
				u.setRole("member");
				try (UserDao d = new UserDao()) {
					d.insert(u);
					System.out.println("Member added!!");

				} catch (Exception e) {
					e.printStackTrace();
				}

				System.out.println();
				break;

			}
			case 4: {
				try (BookDao dao = new BookDao()) {
					Book book = LIbrarian.getBook();
					dao.insert(book);
					System.out.println("New Book added!!");

				} catch (Exception ex) {
					ex.printStackTrace();
				}
				System.out.println();
				break;

			}
			case 5: {
				try (Copiesdao d = new Copiesdao()) {
					System.out.println("Enter the bookid");
					int bookid = sc.nextInt();
					System.out.println("Enter the rack");
					int rack = sc.nextInt();
					d.insert(bookid, rack, "available");
					System.out.println("New Book copy added!!");
				} catch (Exception ex) {
					ex.printStackTrace();

				}
				System.out.println();
				break;

			}
			case 6: {

				Book b = new Book();
				if (b.findBook())
					break;
				else
					System.out.println("Book not found");
				System.out.println();
				break;

			}
			case 7: {
				try (Copiesdao c = new Copiesdao(); IssueRecordDao d = new IssueRecordDao();) {
					System.out.println("Enter the bookid");
					int bookid = sc.nextInt();
					System.out.println("Available copies: ");
					c.getcopyId(bookid);

					System.out.println("Enter the copy id ");
					int copyid = sc.nextInt();
					System.out.println("Enter the user id ");
					int userid = sc.nextInt();

					d.insert(copyid, userid);
					System.out.println("Book copy issued!!");
					c.updatestatus(bookid, "issued");

				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println();
				break;

			}
			case 8: {
				try (Copiesdao c = new Copiesdao(); IssueRecordDao d = new IssueRecordDao();) {
					System.out.println("Enter the bookid");
					int bookid = sc.nextInt();

					System.out.println("Enter the copy id ");
					int copyid = sc.nextInt();
					System.out.println("Enter the user id ");
					int userid = sc.nextInt();

					d.returned(copyid, userid);
					System.out.println("Book copy returned!!");
					c.updatestatus(bookid, "available");
					System.out.println();

				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println();
				break;

			}

			case 9: {

				try (BookDao dao = new BookDao()) {
					System.out.println("Enter the id of the book");
					int bookid = sc.nextInt();
					System.out.println("Enter the price of the book");
					float price = sc.nextFloat();
					dao.update(bookid, price);
					System.out.println("Book details saved!!");

				} catch (Exception ex) {
					ex.printStackTrace();
				}
				System.out.println();
				break;

			}
			case 10: {

				try (PaymentsDao d = new PaymentsDao()) {
					System.out.println("Enter user-Id");
					int userid = sc.nextInt();
					System.out.println("Enter amount");
					int amount = sc.nextInt();
					d.insert(userid, amount);
					System.out.println("Payment received!!");

				} catch (Exception ex) {
					ex.printStackTrace();
				}
				System.out.println();
				break;
			}

			case 11: {

				try (Copiesdao dao = new Copiesdao()) {
					System.out.println("Enter the id of the book");
					int bookid = sc.nextInt();
					List<Integer> list = dao.getcopies(bookid);
					if (list.isEmpty()) {
						System.out.println("Sorry....Book not available");
					} else {
						System.out.println("Book available..!!");
					}

				} catch (Exception ex) {
					ex.printStackTrace();
				}

				System.out.println();
				break;
			}

			case 12: {
				try (Copiesdao dao = new Copiesdao()) {
					System.out.println("Enter the id of the book");
					int bookid = sc.nextInt();
					System.out.println("Enter the new rack of the book");
					int rack = sc.nextInt();
					dao.updateRack(bookid, rack);
					System.out.println("Book rack changed");

				} catch (Exception ex) {
					ex.printStackTrace();
				}
				System.out.println();
				break;
			}

			}
		}
	}

}
