package pojo;

import java.util.List;
import java.util.Scanner;

import dao.Copiesdao;

public class Member extends User {

	public Member() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Member(int userid, String name, String email, String mob_no, String pass, String role) {
		super(userid, name, email, mob_no, pass, role);
		// TODO Auto-generated constructor stub
	}

	public Member(String name, String email, String mob_no, String pass, String role) {
		super(name, email, mob_no, pass, role);
		// TODO Auto-generated constructor stub
	}

	static Scanner sc = new Scanner(System.in);

	public int menulist() {
		System.out.println("0.Sign Out");
		System.out.println("1.Edit Profile");
		System.out.println("2.Edit Password");
		System.out.println("3.Find Book");
		System.out.println("4.Check Avaliablity");

		int choice = sc.nextInt();
		sc.nextLine();

		return choice;
	}

	public void mFunc() {

		int choice = 0;
		while ((choice = menulist()) != 0) {
			switch (choice) {
			case 1: {
				this.editProfile();
				System.out.println();
				break;
			}
			case 2: {
				this.editPassword();
				System.out.println();
				break;
			}

			case 3: {
				Book b = new Book();
				if (b.findBook())
					break;
				else
					System.out.println("Book not found");
				System.out.println();
				break;
			}
			case 4: {
				try (Copiesdao dao = new Copiesdao()) {
					System.out.println("Enter the id of the book");
					int bookid = sc.nextInt();
					List<Integer> list = dao.getcopies(bookid);
					if (list.isEmpty()) {
						System.out.println("Sorry....Book not available");
					} else {
						System.out.println("Book available..!!");
					}

				} catch (Exception ex) {
					ex.printStackTrace();
				}

				System.out.println();
				break;
			}
			}
		}
	}
}
