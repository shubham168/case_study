package test;
import java.util.Scanner;


import dao.UserDao;
import pojo.*;
public class Program {
	private static Scanner sc = new Scanner(System.in);  
	
	
	
	
	public static String[] sign_in() {
		String[] u =new String[2];
		System.out.println("Enter Email  :  ");
		u[0]=sc.nextLine();
		System.out.println("Enter password  :  ");
		u[1]=sc.nextLine();
		return u;
	}
	public static User sign_up() {
		User s = new User();
		System.out.print("Enter your name  :  ");
		s.setName(sc.nextLine());
		System.out.println("Enter Email  :  ");
		s.setEmail(sc.next());
		System.out.println("Enter mob no  :  ");
		s.setMob_no(sc.next());
		System.out.println("create password  :  ");
		s.setPass(sc.next());
		return s;
	}
	
	public static int menuList() {
		System.out.println("0.Exit");
		System.out.println("1.Sign Up");
		System.out.println("2.Sign in");
		System.out.println("Enter Choice");
		int choice=sc.nextInt();
		sc.nextLine();
		return choice;
	}

	public static void main(String[] args) {
		int choice;
		while((choice=menuList())!=0){
			switch(choice) {
			case 1:
				try(UserDao d = new UserDao())
				{
					User u = Program.sign_up();
					u.setRole("owner");
					d.insert(u);
					System.out.println("Sign up succesfully!!!");
				} catch (Exception e) {
					
					e.printStackTrace();
				}
				break;
			case 2:
				try(UserDao d = new UserDao())
				{
					User u = d.getUser(sign_in());
					
					if(u!=null) {
						if (u instanceof Owner) {
							Owner o = (Owner) u;
							o.oFunc();
						}
						
						else if (u instanceof LIbrarian) {
							LIbrarian l = (LIbrarian) u;
							l.lFunc();
						}
						
						else if (u instanceof Member) {
							
							Member m = (Member) u;
							m.mFunc();
						}
						
					}
					
				} catch (Exception e) {
					
					e.printStackTrace();
				}
				break;
			}
		}
	}
	
	
}
