# Library Management Project

## Technologies used

* Java 
* MYSQL (Database)

#

This software can be used by three types of users.
* Owner
* Librarian
* User

## Owner
- Owner owns library.
- He is interested into tracking assets and revenue.
- Functionalities of owner
    ```- Sign in
    - Sign out
    - Appoint librarian
    - Edit profile
    - Edit password
    - Fees report
    - Find report
    - Book categories
    ```

## Librarian 
- Owner appoints a librarian for library management.
- He handles all tasks including data entry, book issue, Payment Collection etc.
- Functionalities of librarian
    ```- Sign in
    - Sign out
    - Edit Profile
    - Edit Password
    - Add member
    - Add new Booklibrarian
    - Add new copy
    - Find Book
    - Issue copy
    - Return copy
    - Edit book
    - Take Payment
    - Check Avaliablity
    - Change rack
    ```

## Member 
- Member is normally a person having paid membership of the library
- Members (reader) can find books and check availability. 
- Functionalities of member
    ```- Sign in
    - Sign out
    - Edit Profile
    - Edit Password
    - Find Book
    - Check Avaliablity
    ```
 

