SET FOREIGN_KEY_CHECKS=0;

DROP TABLE  IF EXISTS users;
DROP TABLE  IF EXISTS payments;
DROP TABLE  IF EXISTS books;
DROP TABLE  IF EXISTS issuerecord;
DROP TABLE  IF EXISTS copies;
SET FOREIGN_KEY_CHECKS=1;
#users table

CREATE TABLE `users` (
  `userid` int NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `email` varchar(30) NOT NULL,
  `mobile_no` varchar(13) DEFAULT NULL,
  `pass` varchar(20) NOT NULL,
  `role` varchar(10) DEFAULT 'member',
  PRIMARY KEY (`userid`)
);


#book table
CREATE TABLE `books`(
    `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `book_name` varchar(30) DEFAULT NULL,
    `author` varchar(30) DEFAULT NULL,
    `subject` varchar(30) DEFAULT NULL,
    `price` DECIMAL(5,2) DEFAULT NULL,
    `isbn` int 
);

#payments table

CREATE TABLE `payments` (
    `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `amount` int ,
  `transaction_time` DATETIME ,
  `nextpayment_duedate` DATE,
  `userid` int NOT NULL,
   FOREIGN KEY(userid)
    REFERENCES users(userid) 
);


#copies table
CREATE TABLE `copies`(
    `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `bookid` int NOT NULL,
    `rack` int,
    `status` varchar(15),
    FOREIGN KEY(bookid)
     REFERENCES books(id)
);


#issuerecord table

CREATE TABLE `issuerecord` (
     `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
     `copyid` int NOT NULL, 
     `memberid` int NOT NULL,
     `issue_date` DATE,
     `return_duedate` DATE,
     `return_date` DATE,
     `fine_amount` int ,
      FOREIGN KEY(memberid) REFERENCES users(userid),
      FOREIGN KEY (copyid) REFERENCES copies(id)
);
